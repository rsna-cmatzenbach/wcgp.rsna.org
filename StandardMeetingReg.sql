
--Example url https://www2stage.rsna.org/shared_society_pages/standard_meeting_reg/?meeting=EC00031
-- OLD ID: 1290038534

-- NEW WRONG ID: 1159190299 (got from EC00031)
-- NEW CORRECT ID: 1290037929 (got from EC00032)
-- (thought event was 031, turns out it was 032)

--STEP 1: Get Product ID of the meeting master product (ex. 1290037929)
select * from product where PARENT_PRODUCT = 'EC00032'

--STEP 2: Insert new config based on an old config
INSERT INTO USR_MTG_CUSTOM_LINE
           ([ID]
           ,[PRODUCT_ID]
           ,[TYPE]
           ,[TEXT]
           ,[ADDOPER]
           ,[ADDDATE]
           ,[MODOPER]
           ,[MODDATE]
           ,[CONCURRENCY_ID]
           ,[DISPLAY_ORDER]
           ,[REQUIRED_FLAG]
           ,[PURCHASING_GROUP]
           ,[QUESTION]
           ,[LANGUAGE])
     select
           row_number() over (order by (select NULL)) + (select max(id) from USR_MTG_CUSTOM_LINE)
           ,1290037929 --this is the new ID from STEP 1
           ,TYPE
           ,TEXT
           ,ADDOPER
           ,CURRENT_TIMESTAMP
           ,NULL
           ,NULL
           ,CONCURRENCY_ID
           ,DISPLAY_ORDER
           ,REQUIRED_FLAG
           ,PURCHASING_GROUP
           ,QUESTION
           ,LANGUAGE
	from USR_MTG_CUSTOM_LINE
  where product_id = 1290038534 --old ID to copy config from

--STEP 3: Make any adjustments to the config.  Some possibilities below.

select * from USR_MTG_CUSTOM_LINE where product_id = '1290037929' order by DISPLAY_ORDER

	update USR_MTG_CUSTOM_LINE set text = '2021-03-22 12:01:00' where product_id = '1290037929' and type = 'DEADLINE_STOP'

  delete USR_MTG_CUSTOM_LINE where type='Banner' and product_id = '1290037929'

  update USR_MTG_CUSTOM_LINE set text = 'NO' where type in('BADGE', 'ADA') and product_id = '1290037929'

  delete from USR_MTG_CUSTOM_LINE where DISPLAY_ORDER in (3, 4, 5, 6, 7) and product_id = '1290037929'

   update USR_MTG_CUSTOM_LINE set text = 'Academic Rank', type = 'TEXT_SHORT', REQUIRED_FLAG = 'N' where display_order = 0 and product_id = '1290037929'

   update USR_MTG_CUSTOM_LINE set text = 'Number of years since completion of training', type = 'TEXT_SHORT', REQUIRED_FLAG = 'N' where display_order = 1 and product_id = '1290037929'

  update USR_MTG_CUSTOM_LINE set text = 'Dietary Restrictions', type = 'TEXT_SHORT', REQUIRED_FLAG = 'N' where display_order = 2 and product_id = '1290037929'

  update USR_MTG_CUSTOM_LINE set text = 'Additional Questions' where product_id = '1290037929' and type = 'questionLabel'

  update USR_MTG_CUSTOM_LINE set text = '<strong>Cancellations</strong><ul><li>All registration cancellations must be made in writing to <a href="mailto:dor@rsna.org">DOR@rsna.org</a>. Any cancellations in writing received between December 15, 2020 and February 1, 2021 will be charged a $75 administrative fee. Refund requests received after February 1, 2021 will not be accepted.</li><li>A minimum of 15 registrants is required for the course to proceed. <strong>If minimum registration is not met by December 15, 2020, the course will be cancelled and the registration fee fully refunded. RSNA <u>will not be responsible</u> for the cost of air fare and/or any associated change fees.</strong></li></ul><br />Questions? Please contact <a href="mailto:dor@rsna.org">DOR@rsna.org</a>.' where product_id = '1290037929' and type = 'FOOTER'

  update USR_MTG_CUSTOM_LINE set text = '<p>----IMPORTANT----<br />A minimum of 15 registrants is required for the course to proceed. If minimum registration is not met by December 15, 2020, the course will be cancelled and the registration fee fully refunded. <strong>We recommend that you do not make flight or hotel reservations until it is confirmed that the course will be held. RSNA <u>will not be responsible</u> for the cost of any air fare, hotel or cancellation charges.</strong></p><p><strong>Hotel Information:</strong> The course will be held at RSNA Headquarters, located at 820 Jorie Boulevard in Oak Brook, Illinois. All related travel and hotel expenses for this course are the participant''s responsibility, and registrants need to make their own hotel reservations. You may want to consider one of the following local Oak Brook hotels for your reservations:</p><p><a href="http://www.marriott.com/hotels/travel/chibk-residence-inn-chicago-oak-brook/" target="_blank">Residence Inn</a> <i>(walking distance to RSNA Headquarters)</i><br />790 Jorie Boulevard, Oak Brook, IL. 60523<br />(P) 630-571-1200</p><p><a href="http://thelodge.hyatt.com/hyatt/hotels-thelodge/index.jsp?hyattprop=yes" target="_blank">Hyatt Lodge</a><br />2815 Jorie Boulevard, Oak Brook, IL. 60523<br />(P) 630-990-5800</p><p><strong>Course Information</strong></p><p><strong>RSVP for Friday Dinner</strong><br />Note that all participants are expected to attend the dinner unless prior notification has been provided to RSNA staff. If you have dietary restrictions, please send them with your RSVP to <a href="mailto:dor@rsna.org">DOR@rsna.org</a>.</p><p>If you will NOT be attending the dinner on Friday evening, please let us know by Monday, March 2, 2021.</p><p><strong>Pre-course Information</strong><br />For pre course information, please contact Tori Peoples at <a href="mailto:dor@rsna.org">DOR@rsna.org</a> or 630-368-3758.</p>' where product_id = '1290037929' and type = 'CONF_FOOTER'

	--inserting new question
	INSERT INTO USR_MTG_CUSTOM_LINE
           ([ID]
           ,[PRODUCT_ID]
           ,[TYPE]
           ,[TEXT]
           ,[ADDOPER]
           ,[ADDDATE]
           ,[MODOPER]
           ,[MODDATE]
           ,[CONCURRENCY_ID]
           ,[DISPLAY_ORDER]
           ,[REQUIRED_FLAG]
           ,[PURCHASING_GROUP]
           ,[QUESTION]
           ,[LANGUAGE])
     select top 1
           row_number() over (order by (select NULL)) + (select max(id) from USR_MTG_CUSTOM_LINE)
       ,1290037929 --this is the new ID from STEP 1
           ,'TEXT_SHORT'
           ,'Academic Rank'
           ,'Chris!'
           ,CURRENT_TIMESTAMP
           ,NULL
           ,NULL
           ,CONCURRENCY_ID
           ,0
           ,'N'
           ,PURCHASING_GROUP
           ,'Y'
           ,LANGUAGE
	from USR_MTG_CUSTOM_LINE
  where product_id = 1290038534 --old ID to copy config from

	--inserting new config var to hide reg info and billing
		INSERT INTO USR_MTG_CUSTOM_LINE
           ([ID]
           ,[PRODUCT_ID]
           ,[TYPE]
           ,[TEXT]
           ,[ADDOPER]
           ,[ADDDATE]
           ,[MODOPER]
           ,[MODDATE]
           ,[CONCURRENCY_ID]
           ,[DISPLAY_ORDER]
           ,[REQUIRED_FLAG]
           ,[PURCHASING_GROUP]
           ,[QUESTION]
           ,[LANGUAGE])
     select top 1
           row_number() over (order by (select NULL)) + (select max(id) from USR_MTG_CUSTOM_LINE)
       ,1290037929 --this is the new ID from STEP 1
           ,'HideRegInfoBilling'
           ,'YES'
           ,'Derrick!'
           ,CURRENT_TIMESTAMP
           ,NULL
           ,NULL
           ,CONCURRENCY_ID
           ,DISPLAY_ORDER
           ,REQUIRED_FLAG
           ,PURCHASING_GROUP
           ,QUESTION
           ,LANGUAGE
	from USR_MTG_CUSTOM_LINE
  where product_id = 1290038534 --old ID to copy config from
